package com.datastructures.stack;

public class ArrayStack<T> {
	
	T[] dataArr;
	int top;
	private int capacity;
	
	@SuppressWarnings("unchecked")
	ArrayStack(int capacity){
		dataArr = (T[]) new Object[capacity];
		top = -1;
		this.capacity = capacity;
	}
	
	public T pop() {
		if(top==-1) {
			System.out.println("stack is empty");
			return null;
		}
		T data = dataArr[top];
		dataArr[top] = null;
		top--;
		return data;
	}
	
	public T peep() {
		if(top==-1) {
			System.out.println("stack is empty");
			return null;
		}
		return dataArr[top];
	}
	public void push (T str) {
		if(top==capacity-1) {
			System.out.println("stack is full");
			return; 
		}
		dataArr[++top] = str; 
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i = top; i>=0 ; i--) {
			stringBuilder.append(dataArr[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(" LENGTH: ");
		stringBuilder.append(top+1);
		
		return stringBuilder.toString();
	}	
	
	
}
