/**
 * 
 */
package com.datastructures.stack;

/**
 * @author anand_raj
 *
 */
public class ArrayStackTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayStack<String> stack = createStack();
		System.out.println(stack);
		System.out.println(stack.peep());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());

	}

	private static ArrayStack<String> createStack() {
		ArrayStack<String> stack = new ArrayStack<>(6);
		stack.push("anand");
		stack.push("syed");
		stack.push("vairam");
		stack.push("girish");
		stack.push("thinesh");
		
		return stack;
	}

}
