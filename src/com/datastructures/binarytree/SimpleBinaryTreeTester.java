package com.datastructures.binarytree;

public class SimpleBinaryTreeTester {

	public static void main(String[] args) {
		SimpleBinaryTree<Integer> binaryTree = new SimpleBinaryTree<>();
		binaryTree.add(5);
		binaryTree.add(7);
		binaryTree.add(9);
		binaryTree.add(4);
		binaryTree.add(10);
		binaryTree.add(2);
		binaryTree.add(6);
		System.out.println("Contains: " + binaryTree.contains(9));
		System.out.println("containsRecursively: " + binaryTree.containsRecursively(9));
	}

}
