/**
 * 
 */
package com.datastructures.binarytree;


/**
 * @author anand_raj
 *
 */
public class SimpleBinaryTree<T extends Comparable<T>> {
	
	private Node root;
	private int size;

	private class Node{
		@SuppressWarnings("unused")
		private Node parent; 
		private Node left; 
		private Node right; 
		private T data;
		
		public Node(T item){
			this.data=item;
		}
	}
	
	public void add(T item) {
		size++;
		Node newNode = new Node(item);	
		if(root==null) {
			root = newNode;
			return;
		}
		associate(root, newNode);
	}
	
	private void associate(Node parent, Node child) {
		if(child.data.compareTo(parent.data)>0) {
			if(parent.right==null) {
				parent.right = child;
				child.parent = parent;
				return;
			}
			associate(parent.right,child);
		} else if(child.data.compareTo(parent.data)<0) {
			if(parent.left==null) {
				parent.left = child;
				child.parent = parent;
				return;
			}
			associate(parent.left,child);
		}
	}

	public boolean contains(T newValue) {
		Node checkNode = new Node(newValue);
		Node currentNode = root;
		while (currentNode != null) {
			int compareResult = checkNode.data.compareTo(currentNode.data);
			if (compareResult == 0)
				return true;
			else if (compareResult > 0)
				currentNode = currentNode.right;
			else
				currentNode = currentNode.left;
		}
		return false;
	}

	private Node contains(Node currentNode, Node checkNode) {
		if(currentNode!=null && checkNode.data.compareTo(currentNode.data)>0) 
			contains(currentNode.right, checkNode);
		else if(currentNode!=null && checkNode.data.compareTo(currentNode.data)<0)
			contains(currentNode.left, checkNode);
		else 
			return currentNode;	//TODO: when it goes inside this block why is the last return statement executed
		return null;
	}
	
	public boolean containsRecursively(T newValue) {
		Node currentNode = root;
		Node checkNode = new Node(newValue);
		Node node = contains(currentNode, checkNode);
		System.out.println("node: " +node);
		return node!=null;
	}
}
