/**
 * 
 */
package com.datastructures.linkedlist;

/**
 * @author anand_raj
 *
 */
public class DoubleLinkedList<T extends Comparable<T>> {

	private Node head;
	
	private int length;

	private Node lastPointer;

	private class Node{
		private T data;
		private Node next;
		private Node prev;
		
		public Node(T item) {
			data = item;
		}
	}
	public DoubleLinkedList () {
		length = 0;
		head = null;
	}
	
	public Node getHead() {
		return head;
	}
	
	public int getLength() {
		return this.length;
	}
	
	
	public void add(T item) {
		Node newNode = new Node(item);
		
		if(head == null)
			head = newNode;
		else {
			Node currentNode = head;
			while(currentNode.next!= null) {
				currentNode = currentNode.next;
			}
			newNode.prev = currentNode;
			currentNode.next = newNode;
		}
		
		length++;
	}
	public void addAtStart(T item) {
		Node newNode = new Node(item);
		if(head == null) 
			head = newNode;
		else {
			newNode.next = head;
			head.prev = newNode;
			head = newNode; 
		}
		length++;
	}

	@Override
	public String toString() {
		if(head == null) 
			return "";
		Node currentNode = head;
		StringBuilder builder = new StringBuilder();
		while(currentNode!=null) {
			builder.append(currentNode.data);
			builder.append(" ");
			currentNode = currentNode.next;
		}
		builder.append(", Length: ");
		builder.append(length);
		return builder.toString();
	}

	public void addAfter(T newNodeValue, T afterNodeValue) {
		Node newNode = new Node(newNodeValue);
		if(head == null) 
			head = newNode;
		else {
			Node currentNode = head;
			while(currentNode != null) {
				if(currentNode.data.equals(afterNodeValue)) {
					newNode.next = currentNode.next;
					currentNode.next = newNode;
					newNode.prev = currentNode;
					newNode.next.prev = newNode;
					break;
				}
				currentNode = currentNode.next;
			}
		}
		length++;
		
		
	}

	public void delete(T item) {
		if(head == null) 
			return;
		Node currentNode = head;	
		if(currentNode.data.equals(item)) {	//First Node
			head = currentNode.next;
			currentNode = null;
			length--;
			return;
		}
		while(currentNode.next!=null) {
			if(currentNode.data.equals(item)) { //Middle Node
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
				currentNode = null;
				length--;
				return;
			}
			currentNode = currentNode.next;
		}
		if(currentNode.data.equals(item)) {	//Last Node
			currentNode.prev.next = null;
			currentNode = null;
			length--;
			return;
		}
	}
	
	public void reverseUsingHead() {
		if(head == null) 
			return;
		Node currentNode = head;
		Node temp ;
		while(currentNode!=null) {
			temp = currentNode.next;
			currentNode.next = currentNode.prev;
			currentNode.prev = temp;
			head = currentNode;
			currentNode = currentNode.prev;
		}
	}

	public void addToSorted(T data) {
		Node newNode = new Node(data);
		if(head==null) {	//Null scenario
			head = newNode;
			length++;
			return;
		}
		if(head.data.compareTo(newNode.data)>0) { //First Node scenario
			newNode.next = head;
			head.prev = newNode;
			head = newNode;	 
			length++;
			return;
		}
		Node currentNode = head.next;
		while(currentNode.next!=null) {
			if(currentNode.data.compareTo(newNode.data)>0) {
				break;
			}
			currentNode = currentNode.next;
		}
		if(currentNode.next!=null) {
			currentNode.prev.next = newNode;
			newNode.prev = currentNode.prev;
			currentNode.prev = newNode;
			newNode.next = currentNode;
		} else {
			currentNode.next = newNode;
			newNode.prev = currentNode;
		}
			
		length++;
			
	}

	public void deleteAt(int position) {
		if(head  == null || position > length)
			return;
		if(position==0) {
			head = head.next ;
			head.prev = null;
		} else {
			Node currentNode = head;
			for(int i=2;i<=position;i++) {
				currentNode = currentNode.next;
			}
			currentNode.prev.next =currentNode.next;
			if(currentNode.next!=null)
				currentNode.next.prev = currentNode.prev;
			currentNode = null;
			
		}
		
		length--;
	}
	public void deleteDuplicates() {
		if(head==null) {
			System.out.println("No records....");
			return;
		}
		Node currentNode = head;
		Node nextNode = head.next;
		while(nextNode!=null && currentNode!=null) {
			if(currentNode.data.equals(nextNode.data)) {
				while(nextNode!=null && currentNode.data.equals(nextNode.data)){
					nextNode = nextNode.next;
					length--;
				}
				currentNode.next = nextNode;
				if(nextNode!=null)
					nextNode.prev = currentNode;
			}
			currentNode = currentNode.next;
			if(nextNode!=null)
				nextNode = nextNode.next;
//			System.out.println("currentNode: "+currentNode.data);
		}
		
	}

	public void sortedInsert(T item) {
		Node newNode = new Node(item);
		if (head == null) {
			head = newNode;
			lastPointer = newNode;
		} else if (newNode.data.compareTo(head.data) < 0) {
			head.prev = newNode;
			newNode.next = head;
			head = newNode;
		} else if (newNode.data.compareTo(lastPointer.data) > 0) {
			lastPointer.next = newNode;
			newNode.prev = lastPointer;
			lastPointer = newNode;
		} else {
			Node currentNode = head.next;
			while (currentNode != null) {
				if (newNode.data.compareTo(currentNode.data) < 0)
					break;
				currentNode = currentNode.next;

			}
			currentNode.prev.next = newNode;
			newNode.prev = currentNode.prev;
			currentNode.prev = newNode;
			newNode.next = currentNode;
		}

		length++;

	}
	public void rotate(int times) {
		if(head == null || times==0)
			return;
		times =  (times >= length) ? times % length: times;	// to reduce the number of rotations
		Node splitNode = head;
		for(int i=1;i<=times;i++) 
			splitNode = splitNode.next;
		Node lastNode = splitNode;
		while(lastNode.next!=null) 
			lastNode = lastNode.next;
		lastNode.next = head;
		head.prev = lastNode;
		head = splitNode;
		splitNode.prev.next = null;
		splitNode.prev = null;
	}
}
