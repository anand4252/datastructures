package com.datastructures.linkedlist;

import java.util.ArrayList;
import java.util.List;

public class CircularSingleLinkedList<T> {
	private Node head;

	private Node last;

	private int length;

	private class Node {

		private T data;

		private Node next;

		public Node(T data) {
			this.data = data;
		}

	}

	public void add(T item) {
		Node newNode = new Node(item);

		if (head == null) {
			head = newNode;
			last = newNode;
			head.next = head;
		} else {
			Node currentNode = last;
			currentNode.next = newNode;

//			Node currentNode = head;
//			while (currentNode.next != head) {
//				currentNode = currentNode.next;
//			}
//			currentNode.next = newNode;
		}
		last = newNode;
		newNode.next = head;
		length++;
	}

	public Node getLast() {
		return last;
	}

	public void setLast(Node last) {
		this.last = last;
	}

	public int getLength() {
		return length;
	}

	public List<CircularSingleLinkedList<T>> split() {
		if (head == null)
			return null;

		Node slow = head;
		Node fast = head;
		while (fast.next != head && fast.next.next != head) {
			slow = slow.next;
			fast = fast.next.next;
		}
		CircularSingleLinkedList<T> firstPart = new CircularSingleLinkedList<>();
		CircularSingleLinkedList<T> secondPart = new CircularSingleLinkedList<>();
		List<CircularSingleLinkedList<T>> splitted = new ArrayList<>();
		Node currentNode = head;
		if (length % 2 == 0) {
			for (; currentNode != slow.next; currentNode = currentNode.next)
				firstPart.add(currentNode.data);

			for (; currentNode != head; currentNode = currentNode.next)
				secondPart.add(currentNode.data);
		} else {
			for (; currentNode != slow; currentNode = currentNode.next)
				firstPart.add(currentNode.data);
			currentNode = currentNode.next;
			for (; currentNode != head; currentNode = currentNode.next)
				secondPart.add(currentNode.data);
		}
		splitted.add(firstPart);
		splitted.add(secondPart);

		return splitted;
	}

	@Override
	public String toString() {
		Node currentNode = head;
		StringBuilder builder = new StringBuilder();
		do {
			builder.append(currentNode.data);
			builder.append(" ");
			currentNode = currentNode.next;
		} while (currentNode != head);
		return builder.toString();
	}

	public boolean isCircuarLinkedList() {
		Node currentNode = head.next;
		boolean isCircuarLinkedList = false;
		while (currentNode != null) {
			currentNode = currentNode.next;
			if (currentNode != head) {
				isCircuarLinkedList = true;
				break;
			}
		}
		return isCircuarLinkedList;
	}

	public CircularSingleLinkedList<T> delete(T item) {
		if (last == null) // Empty Scenario
			return new CircularSingleLinkedList<>();
		Node previousNode = last;
		Node currentNode = last.next;
		if (currentNode == previousNode && currentNode.data.equals(item)) { // Only One Node is present
			currentNode = null;
			return new CircularSingleLinkedList<>();
		}
		while (currentNode != last) {	// first and middle node
			if (currentNode.data.equals(item)) {
				previousNode.next = currentNode.next;
				currentNode = null;
				head =last.next;
				return getUpdatedCircularLinkedList();
			}
			currentNode = currentNode.next;
			previousNode = previousNode.next;
		}
		if(currentNode.data.equals(item)) {	//last Node scenario
			last = previousNode;
			last.next =currentNode.next;
			currentNode = null;
		}
		return this;
	}

	public CircularSingleLinkedList<T> getUpdatedCircularLinkedList() {
		Node currentNode = last;
		CircularSingleLinkedList<T> circularSingleLinkedList = new CircularSingleLinkedList<>();
		while(currentNode.next!=last) {
			circularSingleLinkedList.add(currentNode.data);
			currentNode = currentNode.next;
		}
		circularSingleLinkedList.add(currentNode.data);
		return circularSingleLinkedList;
	}
	
	public int getCount() {
		if(last == null)
			return 0;
		int count  = 1;
		
		if(last == head)
			return count;
		
		Node currentNode = last.next;
		while(currentNode!=last) {
			currentNode = currentNode.next;
			count++;
		}
		return count;
		
		
	}

	public void swap() {
		Node currentNode = head;
		while(currentNode.next.next!=head) {
			currentNode = currentNode.next;
		}
		currentNode.next.next = head.next;
		head.next = currentNode.next;
		currentNode.next = head;
		head = head.next;
	}
}
