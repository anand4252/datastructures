/**
 * 
 */
package com.datastructures.linkedlist;

/**
 * @author anand_raj
 *
 */
public class DoubleLinkedListTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DoubleLinkedList<String> doubleLinkedList = createDoubleLinkedList();
		/**
		 * Add at start
		 */
		doubleLinkedList.addAtStart("amma");
		System.out.println(doubleLinkedList);
		doubleLinkedList.addAfter("hahaha","vairam");
		System.out.println(doubleLinkedList);
		doubleLinkedList = createDoubleLinkedList();
		System.out.println(doubleLinkedList);
		/**
		 * Delete first Node
		 */
		doubleLinkedList.delete("anand");
		System.out.println("Deleted First node: "+ doubleLinkedList);
		/**
		 * Delete middle Node
		 */
		doubleLinkedList.delete("vairam");
		System.out.println("Deleted middle node: "+ doubleLinkedList);
		/**
		 * Delete last Node
		 */
		doubleLinkedList.delete("thinesh");
		System.out.println("Deleted last node: "+ doubleLinkedList);
		
		/**
		 * Reverse using head
		 * ### Revise
		 */
		doubleLinkedList = createDoubleLinkedList();
		doubleLinkedList.reverseUsingHead();
		System.out.println("Reverse using head: "+doubleLinkedList);
		
		/**
		 * Insert in a sorted List
		 */
		DoubleLinkedList<Integer> integerDoubleLinkedList = createIntegerDoubleLinkedList();
		System.out.println("integerDoubleLinkedList: " + integerDoubleLinkedList);
		integerDoubleLinkedList.addToSorted(2);
		System.out.println("integerDoubleLinkedList: " + integerDoubleLinkedList);
		integerDoubleLinkedList.addToSorted(25);
		System.out.println("integerDoubleLinkedList: " + integerDoubleLinkedList);
		integerDoubleLinkedList.addToSorted(55);
		System.out.println("integerDoubleLinkedList: " + integerDoubleLinkedList);
		integerDoubleLinkedList.addToSorted(19);
		System.out.println("integerDoubleLinkedList: " + integerDoubleLinkedList);
		/**
		 * Delete a node at a given position
		 */
		doubleLinkedList = createDoubleLinkedList();
		doubleLinkedList.deleteAt(0);
		System.out.println("Delete at 0(anand): " + doubleLinkedList);
		doubleLinkedList = createDoubleLinkedList();
		doubleLinkedList.deleteAt(3);
		System.out.println("Delete at 3(vairam) : " + doubleLinkedList);
		doubleLinkedList = createDoubleLinkedList();
		doubleLinkedList.deleteAt(5);
		System.out.println("Delete at 5(thinesh): " + doubleLinkedList);
		integerDoubleLinkedList = createIntegerDoubleLinkedList();
		integerDoubleLinkedList.deleteDuplicates();
		System.out.println("Delete Duplicates: "+integerDoubleLinkedList);
		
		/**
		 * Sorted Insert ### Revise
		 */
		DoubleLinkedList<Integer> sortedInsert = new DoubleLinkedList<>();
		sortedInsert.sortedInsert(30);
		sortedInsert.sortedInsert(10);
		sortedInsert.sortedInsert(50);
		sortedInsert.sortedInsert(43);
		sortedInsert.sortedInsert(56);
		sortedInsert.sortedInsert(12);
		System.out.println("Sorted inserts: "+sortedInsert);
		
		/**
		 * Rotate the node
		 * 
		 */
		doubleLinkedList = createDoubleLinkedList();
		doubleLinkedList.rotate(6);
		System.out.println("Rotated 3: "+doubleLinkedList);

	}

	private static DoubleLinkedList<String> createDoubleLinkedList() {
		DoubleLinkedList<String> doubleLinkedList = new DoubleLinkedList<>();
		doubleLinkedList.add("anand");
		doubleLinkedList.add("syed");
		doubleLinkedList.add("vairam");
		doubleLinkedList.add("girish");
		doubleLinkedList.add("thinesh");
		return doubleLinkedList;
	}
	
	private static DoubleLinkedList<Integer> createIntegerDoubleLinkedList() {
		DoubleLinkedList<Integer> doubleLinkedList = new DoubleLinkedList<>();
		doubleLinkedList.add(10);
		doubleLinkedList.add(20);
		doubleLinkedList.add(30);
		doubleLinkedList.add(40);
		doubleLinkedList.add(50);
		return doubleLinkedList;
	}
	
}
