package com.datastructures.linkedlist;

public class SingleLinkedListTester {

	public static void main(String[] args) {
		SingleLinkedList<String> singleLinkedList = createResetSingleLinkedList();
		
		System.out.println("*** Printing ***");
		singleLinkedList.print();
		
		/**
		 * Delete record
		 */
		singleLinkedList.remove("syed");
		System.out.println("*** Printing after deletion ***");
		singleLinkedList.print();
		
		/**
		 * Remove at a position
		 */
		singleLinkedList = createResetSingleLinkedList();
		singleLinkedList.removeAt(0);
		System.out.println("*** Printing after deletion at 1st position ***");
		singleLinkedList.print();
		
		singleLinkedList = createResetSingleLinkedList();
		singleLinkedList.removeAt(2);
		System.out.println("*** Printing after deletion at nth position ***");
		singleLinkedList.print();
		
		/**
		 * Get length iteratively and recursively
		 */
		singleLinkedList = createResetSingleLinkedList();
		System.out.println("Print the length iteratively: "+singleLinkedList.getLengthIteratively());
		System.out.println("Print the length Recursively: "+singleLinkedList.getLengthRecursively());
		
		/**
		 * Get the position of an Item. -1 if it doesn't exist.
		 */
		singleLinkedList = createResetSingleLinkedList();
		System.out.println("Position of anand: "+singleLinkedList.getPositionIteratively("anand"));
		System.out.println("Position of vairam: "+singleLinkedList.getPositionIteratively("vairam"));
		System.out.println("Position of thinesh: "+singleLinkedList.getPositionIteratively("thinesh"));
		System.out.println("Position of poda: "+singleLinkedList.getPositionIteratively("poda"));
		
		
		/**
		 * Get the nth node(Position 0 considered)
		 */
		singleLinkedList = createResetSingleLinkedList();
		System.out.println("Get 0th Node: " + singleLinkedList.getNthNode(0));
		System.out.println("Get 2th Node: " + singleLinkedList.getNthNode(2));
		System.out.println("Get 4th Node: " + singleLinkedList.getNthNode(4));
		System.out.println("Get 5th Node: " + singleLinkedList.getNthNode(5));
		
		/**
		 * nth node from End of the List (Position 0 NOT considered)
		 */
		System.out.println("1st node from the end: "+singleLinkedList.getNthNodeFromEnd(1));
		System.out.println("3rd node from the end: "+singleLinkedList.getNthNodeFromEnd(3));
		System.out.println("5th node from the end: "+singleLinkedList.getNthNodeFromEnd(5));
		System.out.println("6th node from the end: "+singleLinkedList.getNthNodeFromEnd(6));
		
		/**
		 * Get the middle node Iteratively
		 */
		System.out.println("Get the middle node: " + singleLinkedList.getMiddleNode());
		
		/**
		 * Get the middle node using 2 pointers ###REVISE
		 */
		System.out.println("Get the middle node using 2 pointer: " + singleLinkedList.getMiddleNodeUsing2Pointers());
		singleLinkedList.remove("anand");
		System.out.println("Get the middle node using 2 pointer: " + singleLinkedList.getMiddleNodeUsing2Pointers());
		
		/**
		 * No of times a given item is present in a Single linked List
		 */
		singleLinkedList = createResetSingleLinkedList();
		System.out.println("Get repitition count : " + singleLinkedList.noOfRepetition("po"));
		
		/**
		 * ###REVISE
		 * Detect a loop in linked List
		 */
		SingleLinkedList<String> loopedSingleLinkedList = createSingleLinkedListWithLoop();
		System.out.println("***** Is loop Present *****");
		System.out.println("2 pointers with looped Singled Linked List: " + singleLinkedList.isLoopPresentUsing2Pointers(loopedSingleLinkedList));
		System.out.println("2 pointers with normal Singled Linked List: " + singleLinkedList.isLoopPresentUsing2Pointers(singleLinkedList));
		
		System.out.println("Using Hashing (Looped linked list): " + singleLinkedList.isLoopPresentUsingHashing(loopedSingleLinkedList));
		System.out.println("Using Hashing (Normal Singled Linked List): " + singleLinkedList.isLoopPresentUsingHashing(singleLinkedList));
		
		/**
		 * Reverse a linkedListe ###REVISE
		 */
		singleLinkedList = createResetSingleLinkedList();
		System.out.println("Reversed LinkedList : " );
		singleLinkedList.reverse().print();
		/**
		 * Check for Palindrome ##REVISE
		 */
		SingleLinkedList<String> palindromeSingleLinkedList = createPalindrome();
		System.out.println("isPalindromeSingle Using Stack: " + palindromeSingleLinkedList.isPalindromeSingleUsingStack());
		System.out.println("isPalindromeSingle: " + palindromeSingleLinkedList.isPalindrome());
		palindromeSingleLinkedList = new SingleLinkedList<>();
		System.out.println("isPalindromeSingle Using Stack: " + palindromeSingleLinkedList.isPalindromeSingleUsingStack());
		System.out.println("isPalindromeSingle: " + palindromeSingleLinkedList.isPalindrome());
		palindromeSingleLinkedList = createResetSingleLinkedList();
		System.out.println("isPalindromeSingle Using Stack: " + palindromeSingleLinkedList.isPalindromeSingleUsingStack());
		System.out.println("isPalindromeSingle: " + palindromeSingleLinkedList.isPalindrome());
		
		/**
		 * Create Duplicate lists
		 */
		SingleLinkedList<String> duplicateAdjSingleLinkedList =  createWithDuplicateAdjacentRecords();
		duplicateAdjSingleLinkedList.removeAdjacentDuplicates();
		System.out.println("----");
		duplicateAdjSingleLinkedList.print();
		
		
	}
	
	private static SingleLinkedList<String> createWithDuplicateAdjacentRecords() {
		SingleLinkedList<String> myLinkedList = new SingleLinkedList<>();
		myLinkedList.add("11");
		myLinkedList.add("11");
		myLinkedList.add("11");
		myLinkedList.add("21");
		myLinkedList.add("43");
		myLinkedList.add("43");
		myLinkedList.add("60");
		myLinkedList.add("61");
		return myLinkedList;
	}

	private static SingleLinkedList<String> createPalindrome() {
		SingleLinkedList<String> myLinkedList = new SingleLinkedList<>();
		myLinkedList.add("a");
		myLinkedList.add("n");
		myLinkedList.add("d");
		myLinkedList.add("n");
		myLinkedList.add("a");
		System.out.println("Initial no of records: " + myLinkedList.getLength());
		return myLinkedList;
	}
	
	public static SingleLinkedList<String> createResetSingleLinkedList() {
		SingleLinkedList<String> myLinkedList = new SingleLinkedList<>();
		myLinkedList.add("anand");
		myLinkedList.add("syed");
		myLinkedList.add("vairam");
		myLinkedList.add("girish");
		myLinkedList.add("thinesh");
		System.out.println("Initial no of records: " + myLinkedList.getLength());
		return myLinkedList;
	}
	
	private static SingleLinkedList<String> createSingleLinkedListWithLoop() {

		SingleLinkedList<String> myLinkedList = createResetSingleLinkedList();
		
		SingleLinkedList<String>.Node currentNode = myLinkedList.getHead();
		SingleLinkedList<String>.Node vairamNode = null;
		while(currentNode.next!=null) {
			if(currentNode.data.equals("syed"))
				vairamNode = currentNode.next;
			currentNode = currentNode.next;
		}
		currentNode.next = vairamNode;
		return myLinkedList;
	}


}
