package com.datastructures.linkedlist;

import java.util.List;

public class CircularSingleLinkedListTester {

	public static void main(String[] args) {
	
		CircularSingleLinkedList<String> circularSingleLinkedist = createCircularSingleLinkedist();
		/**
		 * Split Circular Linked List
		 */
		List<CircularSingleLinkedList<String>> splitted = circularSingleLinkedist.split();
		System.out.println("--------- Split Circular LinkedList ---------");
		System.out.println("splitted.get(0): " + splitted.get(0));
		System.out.println("splitted.get(1): " + splitted.get(1));
		/**
		 * Is Circular LinkedList
		 */
		System.out.println("--------- Is Circular LinkedList ---------");
		System.out.println("isCircularSingleLinkedist: " + circularSingleLinkedist.isCircuarLinkedList());
		SingleLinkedList<String> singleLinkedist = SingleLinkedListTester.createResetSingleLinkedList();
		System.out.println("isCircularSingleLinkedist: " + singleLinkedist.isCircuarLinkedList());
		
		/**
		 * Delete a given node
		 */
		circularSingleLinkedist = createCircularSingleLinkedist();
		System.out.println("--------- Delete First Node  ---------");
		circularSingleLinkedist.delete("anand");
		System.out.println(circularSingleLinkedist);
		
		System.out.println("--------- Delete Middle Node  ---------");
		circularSingleLinkedist = createCircularSingleLinkedist();
		circularSingleLinkedist.delete("vairam");
		System.out.println(circularSingleLinkedist);
		System.out.println("--------- Delete Last Node  ---------");
		circularSingleLinkedist = createCircularSingleLinkedist();
		circularSingleLinkedist.delete("thinesh");
		System.out.println(circularSingleLinkedist);
		
		/**
		 * GetCount 
		 */
		circularSingleLinkedist = createCircularSingleLinkedist();
		System.out.println("--------- Get count  ---------");
		System.out.println("Full List: "+ circularSingleLinkedist.getCount());
		circularSingleLinkedist = new CircularSingleLinkedList<>();
		System.out.println("Empty List: "+ circularSingleLinkedist.getCount());
		circularSingleLinkedist.add("anand");
		System.out.println("One List: "+ circularSingleLinkedist.getCount());
		
		/**
		 * Swap the first and last node
		 */
		circularSingleLinkedist = createCircularSingleLinkedist();
		System.out.println("--------- Before Swapping≈  ---------");
		System.out.println(circularSingleLinkedist);		
		System.out.println("--------- After Swapping≈  ---------");
		circularSingleLinkedist.swap();
		System.out.println(circularSingleLinkedist);		
	}

	private static CircularSingleLinkedList<String> createCircularSingleLinkedist() {

		CircularSingleLinkedList<String> myLinkedList = new CircularSingleLinkedList<>();
		myLinkedList.add("anand");
		myLinkedList.add("syed");
		myLinkedList.add("vairam");
		myLinkedList.add("girish");
		myLinkedList.add("thinesh");
		System.out.println("Initial no of records: " + myLinkedList.getLength());
		return myLinkedList;
	
		
	}

}
