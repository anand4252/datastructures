/**
 * 
 */
package com.datastructures.linkedlist;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;


/**
 * @author anand_raj
 *
 */
public class SingleLinkedList<T> {
	
	private Node head;
	
	private int length;

	private Node last;

	
	
	public Node getLast() {
		return last;
	}

	public Node getHead() {
		return head;
	}


	public int getLength() {
		return length;
	}
	//Should be changed to private. Right now it is public just to try out an example. 
	public class Node{

		public T data;	
		
		public Node next;
		
		public Node(T data) {
			this.data = data;
		}

	}

	public SingleLinkedList<T> add(T data) {
		Node newNode = new Node(data);
		
		if(head==null) 
			head = newNode;
		else {
			Node currentNode = head;
			
			while(currentNode.next != null) {
			currentNode = currentNode.next;
			}
			currentNode.next = newNode;
			last = newNode;
		}
		length++;
		return this;
	}
	

	public boolean isLoopPresentUsing2Pointers(SingleLinkedList<T> singleLinkedList) {
		if (singleLinkedList == null)
			return false;
		Node fastPointer = singleLinkedList.head;
		Node slowPointer = singleLinkedList.head;
		while(slowPointer !=null && fastPointer != null && fastPointer.next!=null) {
			slowPointer = slowPointer.next;
			fastPointer = fastPointer.next.next;
			if(slowPointer.data.equals(fastPointer.data)) 
				return true;
		}
					
		return false;
	}


	public int noOfRepetition(T item) {
		if (head == null)
			return 0;
		int count = 0;
		for(Node currentNode = head; currentNode!=null; currentNode = currentNode.next) {
			if(currentNode.data.equals(item))
				count++;
		}
		return count;
	}

	public T getMiddleNodeUsing2Pointers() {
		if(head==null)
			return null;
		Node slowPointer = head;
		Node fastPointer = head;
		while(fastPointer!=null && fastPointer.next!=null) {
			fastPointer = fastPointer.next; 
			if(fastPointer==null) {
				break;
			}
			fastPointer = fastPointer.next;
			slowPointer = slowPointer.next;
		}
		
		return slowPointer.data;
	}

	public Node getMiddleNode() {
		int position = getLengthIteratively()/2 + 1;
		return getNthNode(position-1);
	}

	public Node getNthNodeFromEnd(int n) {
		int position = length - n;
		if(position<0)
			return null;
		return getNthNode(position);
	}

	public Node getNthNode(int n) {
		if(length<n+1)
			return null;
		Node currentNode = head;
		for(int i =0 ; i<n ; i++) 
			currentNode = currentNode.next;
		return currentNode;
	}

	public int getPositionIteratively(T searchData) {
		int position = 0;
		Node currentNode = head;
		while(currentNode != null) {
			if(currentNode.data.equals(searchData))
				return position;
			currentNode = currentNode.next;
			position++;
		}
		return -1;
	}



	public int getLengthIteratively() {
		if (head==null)
			return 0;
		int len = 1;
		Node currentNode = head;
		while(currentNode.next!=null) {
			currentNode = currentNode.next;
			len++;
		}
		
		return len;
	}
	
	public int getLengthRecursively(SingleLinkedList<T> list) {
		if (list.head==null)
			return 0;
		this.length ++;
		
		if(head.next!=null) {
			head = head.next;
			getLengthRecursively(list);
		}
		
		return length;
	}
	
	public int getLengthRecursively() {
		return getLengthRecursively(this);
	}

	public void print() {
		var currentNode = head;
		while(currentNode != null) {
			System.out.println(currentNode.data);
			currentNode = currentNode.next;
		}
		System.out.println("Total records: " + this.length);
	}
	
	public boolean remove(T data) {
		if(head == null) 
			return false;
		
		if(head.data.equals(data)) {
			Node newHead = head.next;
			head.next = null;
			head = newHead;
			length--;
			return true;
		}
		Node previousNode = head;
		Node currentNode = head.next;
		while(currentNode != null) {
			if(currentNode.data==data) {
				previousNode.next = currentNode.next;
				currentNode.next = null;
				length--;
				break;
			}
			currentNode = currentNode.next;
			previousNode = previousNode.next; 
		}
		return false;
	}
	
	public boolean removeAt(int position) {
		if(position>=length)
			return false;
		if(position==0) {
			Node newHead = head.next;
			head.next = null;
			head = newHead;
			length--;
			return true;
		}
		Node previous = head;
		Node current = head.next;
		
		for(int index=1;index<position;index++)
		{
			previous = current;
			current = current.next;
		}
		previous.next = current.next;
		current.next = null;
		length--;
		return false;
	}


	public boolean isLoopPresentUsingHashing(SingleLinkedList<T> loopedSingleLinkedList) {
		if(loopedSingleLinkedList==null)
			return false;
		Set<Integer> hashkeys = new HashSet<>();
		Node currentNode = loopedSingleLinkedList.head;
		while(currentNode.next != null) {
			if(!hashkeys.add(currentNode.hashCode()))
				return true;
			
			currentNode = currentNode.next;
		}
		return false;
	}


	public boolean isPalindromeSingleUsingStack() {
		if(head == null)
			return true;

		Stack<T> stack = new Stack<>();
		Node currentNode = head;
		for(;currentNode!= null;currentNode = currentNode.next) {
			stack.push(currentNode.data);
		}
		currentNode = head;
		for(;currentNode!= null; currentNode = currentNode.next) {
			if(!stack.pop().equals(currentNode.data))
				return false;
		}
		return true;
	}

	public boolean isPalindrome() {
		if(head == null)
			return true;
		int len = this.getLength();
		Node middleNode = this.getMiddleNode();
		SingleLinkedList<T> secondPart = new SingleLinkedList<>();
		if(len%2 == 0)
			secondPart.head = middleNode;
		else
			secondPart.head = middleNode.next;
		SingleLinkedList<T> reversedSecondPart = secondPart.reverse();
		Node currentNode = this.head;
		Node secondCurrentNode = reversedSecondPart.head;
		while(currentNode.next!=middleNode) {
			if(!currentNode.data.equals(secondCurrentNode.data)) {
				return false;
			}
			currentNode = currentNode.next;
			secondCurrentNode = secondCurrentNode.next;
		}
		return true;
	}

	public SingleLinkedList<T> reverse() {
		if(head == null)
			return null;
		Node previousNode = null;
		Node currentNode = head;
		while(currentNode.next!=null) {
			Node nextNode = currentNode.next;
			currentNode.next = previousNode;
			previousNode = currentNode;
			currentNode = nextNode;
		}
		head = currentNode;
		head.next = previousNode;
		SingleLinkedList<T> singleLinkedList = new SingleLinkedList<>();
		while(currentNode != null) {
			singleLinkedList.add(currentNode.data);
			currentNode = currentNode.next;
		}
		return singleLinkedList;
	}
	
	public void removeAdjacentDuplicates(){
		if(head==null)
			return;
		Node currentNode = head;
		Node nextNode = currentNode.next;
		while(nextNode!=null) {
			
			if(currentNode.data.equals(nextNode.data)) {
				currentNode.next = nextNode.next;
			}
			currentNode = nextNode;
//			System.out.println(currentNode.data);
			nextNode = nextNode.next;
		}
	}

	public boolean isCircuarLinkedList() {
		Node currentNode = head.next;
		boolean isCircuarLinkedList = false;
		while(currentNode!= null) {
			currentNode = currentNode.next;
			if(currentNode == head) {
				System.err.println(currentNode.data);
				isCircuarLinkedList = true;
				break;
			}
		}
		return isCircuarLinkedList;
	}
}
