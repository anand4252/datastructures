package com.datastructures.hashtable;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LinkedListHashMap<K,V> {
	private LinkedList<HashEntry<K,V>>[] data;
	private int capacity;
	private int size;
	
	@SuppressWarnings("unchecked")
	public LinkedListHashMap(int capacity) {
		this.capacity = capacity;
		data = new LinkedList[capacity];
		size=0;
	}
	
	@SuppressWarnings("hiding")
	private class HashEntry<K,V>{
		private K key;
		private V value;
		
		public HashEntry(K k,V v) {
			key = k;
			value = v;
		}
	}

	public int getSize() {
		return this.size;
	}
	
	private int calculateHashSlot(K key) {
		return (key.hashCode() & 0xfffffff) % capacity;
	}
	
	public void put(K key, V value) {
		size++;
		int hashSlot = calculateHashSlot(key);
		HashEntry<K,V> newHashEntry = new HashEntry<>(key,value);
		if(data[hashSlot]==null){
			LinkedList<HashEntry<K, V>> linkedList = new LinkedList<>();
			linkedList.add(newHashEntry);
			data[hashSlot] =  linkedList;
			return;
		}
		LinkedList<HashEntry<K, V>> linkedList = data[hashSlot];
		Optional<HashEntry<K, V>> findAny = linkedList.stream().filter(h -> h.key.equals(key)).findAny();
		
		if(findAny.isPresent()) {	//if not match
			size--;
			findAny.ifPresent(s -> s.value=value);
		}
		else
			linkedList.add(newHashEntry);
	}
	
	public V get(K key) {
		if(size==0) {
			System.out.println("HashTable is empty");
			return null;
		}
		int hashSlot = calculateHashSlot(key);
		LinkedList<HashEntry<K, V>> linkedList = data[hashSlot];
		Optional<HashEntry<K, V>> findAny = linkedList.stream().filter(h -> h.key.equals(key)).findAny();
		if(findAny.isPresent())
				return findAny.get().value;
		else
			return null;
	}
	
	public boolean delete(K key) {
		if(key==null)
			return false;
		int hashSlot = calculateHashSlot(key);
		LinkedList<HashEntry<K, V>> linkedList = data[hashSlot];
		if(linkedList!=null && !linkedList.isEmpty()) {
			Optional<HashEntry<K, V>> findFirst = linkedList.stream().filter(hashEntry -> hashEntry.key.equals(key)).findFirst();
			if(findFirst.isPresent()) {
				linkedList.remove(findFirst.get());
				size--;
				return true;
			}
		}
		return false;
	}
	
	public boolean hashKey(K key) {
		if(key==null)
			return false;
		int hashSlot = calculateHashSlot(key);
		LinkedList<HashEntry<K, V>> linkedList = data[hashSlot];
		return linkedList!=null && !linkedList.isEmpty() && linkedList.stream().anyMatch(hashEntry -> hashEntry.key.equals(key));
	}
	
	public boolean hashValue(V value) {
		List<LinkedList<HashEntry<K, V>>> asList = Arrays.asList(data);
		return asList.stream().flatMap(Collection::stream).anyMatch(hashEntry -> hashEntry.value.equals(value));
	}
	

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		List<LinkedList<HashEntry<K, V>>> asList = Arrays.asList(data);
		asList.stream().flatMap(Collection::stream).forEach(hashEntry ->{
			stringBuilder.append("Key: ");
			stringBuilder.append(hashEntry.key);
			stringBuilder.append(" ,Value: " + hashEntry.value);
			stringBuilder.append("\n");
		});
		stringBuilder.append("Size: ");
		stringBuilder.append(size);
		return stringBuilder.toString();
	}
	
	
}
