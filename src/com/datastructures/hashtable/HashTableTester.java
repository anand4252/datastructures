package com.datastructures.hashtable;

public class HashTableTester {

	public static void main(String[] args) {
		BasicHashTable<String, String> myHashTable = new BasicHashTable<>(5);
		
		myHashTable.put("anand", "1");
		myHashTable.put("syed", "2");
		myHashTable.put("vairam", "3");
		myHashTable.put("girish", "4");
		myHashTable.put("thinesh", "5");
		myHashTable.put("anand", "666");
		myHashTable.put("appp", "3");

//		System.out.println("Girish: " + myHashTable.get("girish"));
//		System.out.println("anand: " + myHashTable.get("anand"));
		System.out.println("random: " + myHashTable.get("3289238"));
	}

}
