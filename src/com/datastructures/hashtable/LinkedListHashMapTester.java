package com.datastructures.hashtable;

public class LinkedListHashMapTester {

	public static void main(String[] args) {

		LinkedListHashMap<String, String> myHashTable = new LinkedListHashMap<>(5);
		
		myHashTable.put("anand", "1");
		myHashTable.put("syed", "2");
		myHashTable.put("vairam", "3");
		myHashTable.put("girish", "4");
		myHashTable.put("thinesh", "5");
		myHashTable.put("anand", "666");//Testing override
		myHashTable.put("appu", "3");

		System.out.println("Girish: " + myHashTable.get("girish"));
		System.out.println("random: " + myHashTable.get("prema")); // when the key is not present
		/**
		 * print all the keys and values
		 */
		System.out.println(myHashTable.toString());
		
		/**
		 * Delete Functionality
		 */
		System.out.println("Delete: " + myHashTable.delete("syed"));
		System.out.println(myHashTable.toString());
		/**
		 * HasKey
		 */
		System.out.println("Is Hash key Present: "+ myHashTable.hashKey("chitos"));
		/**
		 * HasValue
		 */
		System.out.println("Is Hash Value Present: "+ myHashTable.hashValue("6666"));

	}

}
