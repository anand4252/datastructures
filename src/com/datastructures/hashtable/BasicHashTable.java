package com.datastructures.hashtable;

public class BasicHashTable<K,V> {
	private HashEntry<K,V>[] data;
	private int capacity;
	private int size;
	
	@SuppressWarnings("unchecked")
	public BasicHashTable(int capacity) {
		this.capacity = capacity;
		data = new HashEntry[capacity];
		size=0;
	}
	
	private class HashEntry<K,V>{
		private K key;
		private V value;
		
		public HashEntry(K k,V v) {
			key = k;
			value = v;
		}
	}

	public int getSize() {
		return this.size;
	}
	
	private int calculateHashSlot(K key) {
		int hashSlot = (key.hashCode() & 0xfffffff) % capacity;
		while(data[hashSlot]!=null && !key.equals(data[hashSlot].key)) { // if the keys are same, don't increment the hashSlot. Instead overwrite the value as they are same.
			System.out.println("Inside for loop: " + hashSlot );
			hashSlot = (hashSlot +1) % capacity;
		}
		return hashSlot;
	}
	
	public void put(K key, V value) {
		if(capacity<size) {
			System.out.println("HashTable is full");
			return;
		}
		
		int hashSlot = calculateHashSlot(key);
		HashEntry<K,V> hashEntry = new HashEntry<>(key,value);
		data[hashSlot] = hashEntry;
		size++;
	}
	
	public V get(K key) {
		if(size==0) {
			System.out.println("HashTable is empty");
			return null;
		}
		int hashSlot = calculateHashSlot(key);
		System.out.println("hashSlot: " + hashSlot);
		V value = null;
		HashEntry<K,V> hashEntry  = null;
		if(data[hashSlot]!=null) {
			hashEntry = data[hashSlot];
			if(hashEntry.key.equals(key))
				value = hashEntry.value;
		}
		
		return value;
	}
}
