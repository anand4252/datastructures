/**
 * 
 */
package com.datastructures.queues;

/**
 * @author anand_raj
 *
 */
public class ListBackedQueueTester {

	public static void main(String[] args) {
		ListBackedQueue<String> queue = createQueue();
		System.out.println(queue);
		queue.enqueue("poda");
		System.out.println("Added poda: "+ queue);
		queue.dequeue();
		System.out.println(queue);
		
	}

	private static ListBackedQueue<String> createQueue() {
		ListBackedQueue<String> queue = new ListBackedQueue<>();
		queue.enqueue("anand");
		queue.enqueue("syed");
		queue.enqueue("vairam");
		queue.enqueue("girish");
		queue.enqueue("thinesh");
		return queue;
	}

}
