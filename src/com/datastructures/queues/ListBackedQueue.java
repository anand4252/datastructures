/**
 * 
 */
package com.datastructures.queues;

import java.util.ArrayList;
import java.util.List;

/**
 * @author anand_raj
 *
 */
public class ListBackedQueue<T> {
	List<T> data;
	int front;
	int rear;

	ListBackedQueue(){
		data = new ArrayList<>();
		front = 0 ;
		rear = -1;
	}
	
	public int getLength() {
		return rear-front + 1;
	}
	
	public void enqueue(T item) {
		data.add(item);
		rear++;
	}
	
	public T dequeue() {
		System.out.println("rear: " +rear);
		if(rear-front<0) {
			System.out.println("Nothing to return");
			return null;
		}
		System.out.println("data.size: " + data.size());
		System.out.println("Item to be dequed:" + front);
		T item = data.get(0);
		data.remove(0);
		front++;
		return item;
	}
	
	public T peek() {
		if(data.isEmpty())
			System.out.println("Nothing to return");
		return  data.get(front);
	}
	
	public boolean isEmpty() {
		return getLength()<=0;
	}

	@Override
	public String toString() {
		System.out.println("Length: "+getLength());
		StringBuilder builder = new StringBuilder();
		int length = getLength();
		for(int i=0;i<=length-1;i++) {
			builder.append(data.get(i));
			builder.append(", ");
		}
		builder.append("Length: ");
		builder.append(this.getLength());
		return builder.toString();
	}
	
}
