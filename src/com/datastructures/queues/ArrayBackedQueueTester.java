package com.datastructures.queues;

public class ArrayBackedQueueTester {

	public static void main(String[] args) {
		ArrayQueue<String> arrayQueue = createQueue();
		System.out.println("Initial: "+arrayQueue);
		arrayQueue.enqueue("appu");
		System.out.println(arrayQueue);
		arrayQueue.dequeue();
		arrayQueue.dequeue();
		System.out.println(arrayQueue);
		arrayQueue.enqueue("chitos");
		arrayQueue.enqueue("kutos");
		arrayQueue.enqueue("patts");
		arrayQueue.enqueue("cookie");
		System.out.println(arrayQueue);
	}

	private static ArrayQueue<String> createQueue() {
		ArrayQueue<String> arrayBackedQueue= new ArrayQueue<>(6);
		arrayBackedQueue.enqueue("anand");
		arrayBackedQueue.enqueue("syed");
		arrayBackedQueue.enqueue("vairam");
		arrayBackedQueue.enqueue("girish");
		arrayBackedQueue.enqueue("thinesh");
		return arrayBackedQueue;
	}
}
