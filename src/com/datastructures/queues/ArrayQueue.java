/**
 * 
 */
package com.datastructures.queues;

/**
 * @author anand_raj
 *
 */
public class ArrayQueue<T> {
	T[] dataArr;
	int front;
	int rear;
	int capacity;
	
	@SuppressWarnings("unchecked")
	public ArrayQueue(int capacity) {
		this.capacity = capacity;
		dataArr = (T[]) new Object[capacity];
		front = 0;
		rear = -1;
	}
	
	public int getLength() {
		return rear-front +1;
	}
	
	public void enqueue(T str) {
		if(rear==capacity-1) {
			System.out.println("Queue is sooo full");
			return;
		}
		dataArr[++rear]= str;
	}
	
	public T dequeue() {
		if(getLength()<0) {
			System.out.println("Queue is empty");
			return null;
		}
		T string = dataArr[front];
		rearrange();	//This make a queue a circular queue.
		return string;
	}

	private void rearrange() {
		for(int i=0;i<rear;i++) {
			dataArr[i]=dataArr[i+1];
		}
		rear--;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(int i=front;i<=rear;i++) {
			builder.append(dataArr[i]);
			builder.append(", ");
		}
		builder.append(" LENGTH: ");
		builder.append(getLength());
		return builder.toString();
	}
	

}
