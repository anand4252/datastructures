package com.datastructures.queues;

public class LinkedListBlockingQueueTest {

	public static void main(String[] args) {
		LinkedListBlockingQueue<String> blockingQueue = createBlockingQueue();
		System.out.println(blockingQueue);
		//Scenario 1
		Thread addThread = new Thread(() ->blockingQueue.enqueue("thanos"));
		Thread removeThread = new Thread(() -> {
			String deletedItem = blockingQueue.dequeue();
			System.out.println("deletedItem: "+ deletedItem);
		});
		try {
			addThread.start();
			Thread.sleep(1500);
			removeThread.start();
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(blockingQueue);
		
		System.out.println("===== Scenario 2 =====");
		LinkedListBlockingQueue<String> blockingQueue2 = new LinkedListBlockingQueue<>(3);
		Thread removeThread2 = new Thread(()->  {
			String dequeuedElement = blockingQueue2.dequeue();
			System.out.println("dequeuedElement: " + dequeuedElement);
		});
		Thread addThread2 = new Thread(() ->blockingQueue2.enqueue("thanos"));
		try {
			removeThread2.start();
			Thread.sleep(1500);
			addThread2.start();
			Thread.sleep(500);
//			removeThread2.join();
//			addThread2.join();
		} catch (InterruptedException e) {
		}
		
		System.out.println(blockingQueue2);
		
	}

	private static LinkedListBlockingQueue<String> createBlockingQueue() {
		LinkedListBlockingQueue<String> blockingQueue = new LinkedListBlockingQueue<>(3);
		blockingQueue.enqueue("thor");
		blockingQueue.enqueue("ironman");
		blockingQueue.enqueue("hulk");
		
		return blockingQueue;
	}
}
