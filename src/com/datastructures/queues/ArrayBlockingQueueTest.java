package com.datastructures.queues;

public class ArrayBlockingQueueTest {
	
	public static void main(String[] args) throws InterruptedException {
		ArrayBlockingQueue<String> arrayBlockingQueue = createArrayBlockingQueue();
		
//		ArrayBlockingQueueTest.addToFullArr(arrayBlockingQueue);
		
		arrayBlockingQueue = new ArrayBlockingQueue<>(5);
		ArrayBlockingQueueTest.dequeueFromEmptyQueue(arrayBlockingQueue);
		
		Thread.sleep(1000);
		System.out.println(arrayBlockingQueue.toString());
		System.out.println("== program ends ==");
	}

	private static void dequeueFromEmptyQueue(ArrayBlockingQueue<String> arrayBlockingQueue) {
		Thread removeThread1 = new Thread(() ->  {
			String dequeuedItem = arrayBlockingQueue.dequeue();
			System.out.println("dequeued Item is: " + dequeuedItem);
		}, "removeThread1");
		Thread removeThread2 = new Thread(() ->  {
			String dequeuedItem = arrayBlockingQueue.dequeue();
			System.out.println("dequeued Item is: " + dequeuedItem);
		}, "removeThread2");
		removeThread1.start();
		removeThread2.start();
		
		Thread addThread1 = new Thread(() ->  arrayBlockingQueue.enqueue("ironman"),"addThread1");
		Thread addThread2 = new Thread(() ->  arrayBlockingQueue.enqueue("hulk"),"addThread2");
		addThread1.start();
		addThread2.start();
	}

	private static void addToFullArr(ArrayBlockingQueue<String> arrayBlockingQueue) throws InterruptedException {
		Thread addThread1 = new Thread(() ->  arrayBlockingQueue.enqueue("ironman"),"addThread1");
		Thread addThread2 = new Thread(() ->  arrayBlockingQueue.enqueue("hulk"),"addThread2");
		Thread removeThread1 = new Thread(arrayBlockingQueue::dequeue, "removeThread1");
		Thread removeThread2 = new Thread(arrayBlockingQueue::dequeue, "removeThread2");
		addThread1.start();
		addThread2.start();
		Thread.sleep(500);
		removeThread1.start();
		removeThread2.start();
		
	}

	private static ArrayBlockingQueue<String> createArrayBlockingQueue() {
		ArrayBlockingQueue<String> arrayBlockingQueue =  new ArrayBlockingQueue<>(5);
		arrayBlockingQueue.enqueue("anand");
		arrayBlockingQueue.enqueue("syed");
		arrayBlockingQueue.enqueue("vairam");
		arrayBlockingQueue.enqueue("girish");
		arrayBlockingQueue.enqueue("thinesh");
		
		return arrayBlockingQueue;
	}

}
