package com.datastructures.queues;

public class ArrayBlockingQueue<T> {
	private T[] data;
	int front;
	int rear;
	int capacity;
	
	
	@SuppressWarnings("unchecked")
	public ArrayBlockingQueue(int capacity) {
		super();
		rear=-1;
		this.capacity = capacity;
		this.data = (T[]) new Object[this.capacity];
	}
	
	public int getSize() {
		return rear - front +1;
	}
	
	public void enqueue(T item) {
		if(rear == capacity-1) {
			System.out.println("Queue is full already...Please wait...");
			synchronized (this) {
				try {
					wait();
					System.out.println("Now data is being added...");
					data[++rear] = item;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return;
			}
		}
		synchronized (this) {
			data[++rear] = item;
			System.out.println("New data is added....");
			notify();
		}
	}
	
	public T dequeue() {
		T item = null;
		if(rear == -1) {
			System.out.println("Queue is empty: Please wait...");
			synchronized (this) {
				try {
					wait();
					item = data[front];
					rearrange();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return item;
		}
		synchronized (this) {
			item = data[front];
			rearrange();
			System.out.println("removeddd");
			notify();
		}
		return item;
	}
	
	private void rearrange() {
		int current = 0;
		while(current<rear) {
			data[current] = data[current+1];
			current++;
		}
		rear--;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("\nPrinting items: ");
		for(int i=front;i<=rear;i++) {
			stringBuilder.append(data[i]);
			stringBuilder.append(", ");
		}
		stringBuilder.append(" SIZE: ");
		stringBuilder.append(getSize());
		return stringBuilder.toString();
	}

	
	

}
