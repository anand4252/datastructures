package com.datastructures.queues;

import java.util.LinkedList;

public class LinkedListBlockingQueue<T> {

	private LinkedList<T> blockingQueue;
	private int capacity;

	public LinkedListBlockingQueue(int capacity) {
		super();
		this.capacity = capacity;
		blockingQueue = new LinkedList<>();
	}

	public void enqueue(T item) {
		if (blockingQueue.size() == this.capacity) {
			synchronized (this) {
				System.out.println("Queue is full already..Please wait..");
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		synchronized (this) {
			blockingQueue.add(item);
			notifyAll();
		}
	}

	public T dequeue() {
		T removedItem = null;
		if (blockingQueue.isEmpty()) {
			synchronized (this) {
				System.out.println("Queue is Empty....Please wait again...");
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		synchronized (this) {
			removedItem = blockingQueue.removeFirst();
			notifyAll();
		}
		return removedItem;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Printing the content: ");
		synchronized (this) {
			blockingQueue.stream().forEach(System.out::println);
		}
		return String.valueOf(blockingQueue.size());
	}

}
